


DROP TABLE IF EXISTS `wp_snp_category`;

CREATE TABLE `wp_snp_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `categoryID` varchar(250) NOT NULL,
  `category` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `wp_snp_category` */

insert  into `wp_snp_category`(`id`,`categoryID`,`category`) values (1,'11111','category 1'),(2,'2222','category 2');
