<?php
/*
Plugin Name: SNP Category Management
Description:
Version: 1
Author: Xie
Author URI: 
*/
// function to create the DB / Options / Defaults					
function ss_category_options_install() {

    global $wpdb;

    $table_name = $wpdb->prefix . "snp_category";
    $charset_collate = $wpdb->get_charset_collate();
	$sql = "DROP TABLE $table_name;CREATE TABLE $table_name (
            `id` int(10) NOT NULL AUTO_INCREMENT,
			  `category` varchar(250) NOT NULL,
			  PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);
	
}

// run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'ss_category_options_install');

//menu items
add_action('admin_menu','snp_category_crud_modifymenu');
function snp_category_crud_modifymenu() {
	
	//this is the main item for the menu
	add_menu_page('SNP Categories', //page title
	'SNP Categories', //menu title
	'manage_options', //capabilities
	'snp_category_crud_list', //menu slug
	'snp_category_crud_list' //function
	);
	
	//this is a submenu
	add_submenu_page('snp_category_crud_list', //parent slug
	'Add SNP Category', //page title
	'Add SNP Category', //menu title
	'manage_options', //capability
	'snp_category_crud_create', //menu slug
	'snp_category_crud_create'); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Update Category', //page title
	'Update', //menu title
	'manage_options', //capability
	'snp_category_crud_update', //menu slug
	'snp_category_crud_update'); //function
}
define('ROOTCATEGORYDIR', plugin_dir_path(__FILE__));
require_once(ROOTCATEGORYDIR . 'snp-category-list.php');
require_once(ROOTCATEGORYDIR . 'snp-category-create.php');
require_once(ROOTCATEGORYDIR . 'snp-category-update.php');
