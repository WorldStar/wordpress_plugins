<?php

function snp_category_crud_create() {
    $categoryID = $_POST["categoryID"];
    $category = $_POST["category"];
    //insert
    global $wpdb;
    if (isset($_POST['insert'])) {
        $table_name = $wpdb->prefix . "snp_category";
        $wpdb->insert(
                $table_name, //table
                array('category' => $category), //data
                array('%s') //data format			
        );
        $message.="Category inserted";
    }
	
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/snp_category_crud/style-admin.css" rel="stylesheet" />
    <div class="wrap" style="margin-left:15%;width:70%">
        <h2>Add New Category</h2>
        <?php if (isset($message)): ?><div class="updated"><p><?php echo $message; ?></p></div><?php endif; ?>
        <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            <!--<p>Three capital letters for the ID</p>-->
            <table class='wp-list-table widefat fixed' style="width:100%">
                <tr>
                    <th class="ss-th-width">Category</th>
                    <td><input type="text" name="category" value="<?php echo $category; ?>" class="ss-field-width" /></td>
                </tr>
            </table>
            <a href="<?php echo admin_url('admin.php?page=snp_category_crud_list') ?>" style="padding-top:10px; margin-right:20px;">&laquo; Back to Category list</a>
            <input type='submit' name="insert" value='Save' class='button'>
        </form>
    </div>
    <?php
}