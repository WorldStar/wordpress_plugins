<?php

function snp_category_crud_list() {
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/snp_category_crud/style-admin.css" rel="stylesheet" />
    <div class="wrap" style="margin-left:15%;width:70%">
        <h2>SNP Categories</h2>
        <div class="tablenav top">
            <div class="alignleft actions" style="float:right;">
                <a href="<?php echo admin_url('admin.php?page=snp_category_crud_create'); ?>">Add Category</a>
            </div>
            <br class="clear">
        </div>
        <?php
        global $wpdb;
        $table_name = $wpdb->prefix . "snp_category";

        $rows = $wpdb->get_results("SELECT id, category from $table_name");
        ?>
        <table class='wp-list-table widefat fixed striped posts' style="width:100%">
            <tr>
                <th class="manage-column ss-list-width" style="width:20%;text-align:center">Category ID</th>
                <th class="manage-column ss-list-width" style="width:30%;text-align:center">category</th>
                <th class="manage-column ss-list-width" style="width:20%;text-align:center">&nbsp;</th>
            </tr>
            <?php foreach ($rows as $row) { ?>
                <tr style="border-bottom-style:inset;border-color:#aaa;">
                    <td class="manage-column ss-list-width" style="text-align:center"><?php echo $row->id; ?></td>
                    
                    <td class="manage-column ss-list-width" style="text-align:center"><?php echo $row->category; ?></td>
                    <td style="text-align:center"><a href="<?php echo admin_url('admin.php?page=snp_category_crud_update&id=' . $row->id); ?>">Update</a></td>
                </tr>
            <?php } ?>
        </table>
    </div>
    <?php
}