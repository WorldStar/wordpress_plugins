<?php

function snp_category_crud_update() {
    global $wpdb;
    $table_name = $wpdb->prefix . "snp_category";
    $id = $_GET["id"];
    $category = $_POST["category"];
	
    $categoryID = $_POST["categoryID"];
	if(!isset($_POST['update'])){
		$category1 = $wpdb->get_results($wpdb->prepare("SELECT category from $table_name where id=%s", $id));
		foreach ($category1 as $cat) {
			$categoryID = $cat->categoryID;
			$category = $cat->category;
		}
	}
//update
    if (isset($_POST['update'])) {
		$wpdb->query("UPDATE $table_name SET category='$category' WHERE id=$id");
        
    }
//delete
    else if (isset($_POST['delete'])) {
        $wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE id = %s", $id));
    } else {//selecting value to update	
        $snps = $wpdb->get_results($wpdb->prepare("SELECT id, category from $table_name where id=%s", $id));
        foreach ($snps as $s) {
            $name = $s->name;
        }
    }
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/snp_category_crud/style-admin.css" rel="stylesheet" />
    <div class="wrap" style="margin-left:15%;width:70%">
        <h2>SNP Category</h2>

        <?php if ($_POST['delete']) { ?>
            <div class="updated"><p>Category deleted</p></div>
            <a href="<?php echo admin_url('admin.php?page=snp_category_crud_list') ?>">&laquo; Back to Category list</a>

        <?php } else if ($_POST['update']) { ?>
            <div class="updated"><p>Category updated</p></div>
            <a href="<?php echo admin_url('admin.php?page=snp_category_crud_list') ?>">&laquo; Back to Category list</a>

        <?php } else { ?>
            <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
                <table class='wp-list-table widefat fixed' style="width:100%">
                    
                    <tr><th>category</th><td>
					<input type="text" name="category" id="category" value="<?php echo $category; ?>"/>
					</td></tr>
                </table>
                <input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
                <input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Do you want really to delete this category?')">
            </form>
        <?php } ?>

    </div>
    <?php
}