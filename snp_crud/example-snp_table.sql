
DROP TABLE IF EXISTS `wp_snp`;

CREATE TABLE `wp_snp` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `snpID` varchar(250) NOT NULL,
  `includeInReport` varchar(5) NOT NULL DEFAULT '0',
  `categoryID` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categoryid` (`categoryID`),
  CONSTRAINT `categoryid` FOREIGN KEY (`categoryID`) REFERENCES `wp_snp_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `wp_snp` */

insert  into `wp_snp`(`id`,`snpID`,`includeInReport`,`categoryID`) values (26,'23233','on',1),(27,'55555555','on',1),(28,'4343','off',2);
