<?php
/*
Plugin Name: SNP Management
Description:
Version: 1
Author: Xie
Author URI: 
*/
// function to create the DB / Options / Defaults					
function ss_options_install() {

    global $wpdb;

    $table_name = $wpdb->prefix . "snp";
    $charset_collate = $wpdb->get_charset_collate();
	$sql = "DROP TABLE $table_name;CREATE TABLE $table_name (
            `id` int(10) NOT NULL AUTO_INCREMENT,
			`snpID`  varchar(250) NOT NULL,
			`includeInReport` varchar(5) NOT NULL DEFAULT '0',
			`categoryID` int(10) NOT NULL DEFAULT '0',
			PRIMARY KEY (`id`,`snpID`),
			KEY `categoryid` (`categoryID`),
			CONSTRAINT `categoryid` FOREIGN KEY (`categoryID`) REFERENCES `wp_snp_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);
	
}

// run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'ss_options_install');

//menu items
add_action('admin_menu','snp_crud_modifymenu');
function snp_crud_modifymenu() {
	
	//this is the main item for the menu
	add_menu_page('SNPs', //page title
	'SNPs', //menu title
	'manage_options', //capabilities
	'snp_crud_list', //menu slug
	'snp_crud_list' //function
	);
	
	//this is a submenu
	add_submenu_page('snp_crud_list', //parent slug
	'Add SNP', //page title
	'Add SNP', //menu title
	'manage_options', //capability
	'snp_crud_create', //menu slug
	'snp_crud_create'); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Update SNP', //page title
	'Update', //menu title
	'manage_options', //capability
	'snp_crud_update', //menu slug
	'snp_crud_update'); //function
}
define('ROOTDIR', plugin_dir_path(__FILE__));
require_once(ROOTDIR . 'snp-list.php');
require_once(ROOTDIR . 'snp-create.php');
require_once(ROOTDIR . 'snp-update.php');
