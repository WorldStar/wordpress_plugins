<?php

function snp_crud_create() {	
	@ini_set('display_errors', 0);
    $snpID = $_POST["snpID"];
    $includeInReport = $_POST["includeInReport"];
    $categoryID = $_POST["categoryID"];
    //insert
    global $wpdb;
    $table_name = $wpdb->prefix . "snp";
    if (isset($_POST['insert'])) {
		if(!isset($_POST["includeInReport"]))$includeInReport = 'off';
        $snp = $wpdb->get_row($wpdb->prepare("SELECT * from $table_name where snpID='$snpID'"));		
		if(!empty($snp)){
			$message.="This SNP already existed.";
		}else{
			$wpdb->insert(
					$table_name, //table
					array('snpID' => $snpID, 'includeInReport' => $includeInReport, 'categoryID' => $categoryID), //data
					array('%s', '%s', '%s') //data format			
			);
			$message.="SNP inserted";
		}
    }
    $table_category_name = $wpdb->prefix . "snp_category";
	$categories = $wpdb->get_results($wpdb->prepare("SELECT id, category from $table_category_name"));
	$rows = $wpdb->get_results("SELECT s.id from $table_name as s INNER JOIN $table_category_name as c ON s.categoryID=c.id");
	
    $totoalcnt = count($rows);
	$limit = 5;
	$maxpages = ceil($totoalcnt / $limit);
	echo $maxpages;
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/snp_crud/style-admin.css" rel="stylesheet" />
    <div class="wrap" style="margin-left:15%;width:70%">
        <h2>Add New SNP</h2>
        <?php if (isset($message)): ?><div class="updated"><p><?php echo $message; ?></p></div><?php endif; ?>
        <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            <!--<p>Three capital letters for the ID</p>-->
            <table class='wp-list-table widefat fixed' style="width:100%">				
                <tr>
                    <th class="ss-th-width">SNP</th>
                    <td><input type="text" name="snpID" value="<?php echo $snpID; ?>" class="ss-field-width" /></td>
                </tr>
                <tr>
                    <th class="ss-th-width">include In Report?</th>
                    <td><input type="checkbox" name="includeInReport" class="ss-field-width" /> Include</td>
                </tr>
                <tr>
                    <th class="ss-th-width">Category</th>
                    <td>
					<select name="categoryID" class="ss-field-width">
						<?php foreach( $categories as $category){?>
							<option value="<?php echo $category->id; ?>"><?php echo $category->category;?></option>
						<?php } ?>
					</select>					
					</td>
                </tr>
            </table>
            <a href="<?php echo admin_url('admin.php?page=snp_crud_list&paged='.$maxpages) ?>" style="padding-top:10px; margin-right:20px;">&laquo; Back to SNP list</a>
            <input type='submit' name="insert" value='Save' class='button'>
        </form>
    </div>
    <?php
}