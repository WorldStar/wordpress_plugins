<?php

function snp_crud_list() {
	if(isset($_GET['paged'])){
		$paged = $_GET["paged"];
	}else{
		$paged = 1;
	}
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/snp_crud/style-admin.css" rel="stylesheet" />
    <div class="wrap" style="margin-left:15%;width:70%">
        <h2>SNPS</h2>
        <div class="tablenav top">
            <div class="alignleft actions" style="float:right;">
                <a href="<?php echo admin_url('admin.php?page=snp_crud_create'); ?>">Add SNP</a>
            </div>
            <br class="clear">
        </div>
        <?php
        global $wpdb;
        $table_name = $wpdb->prefix . "snp";
		$table_category_name = $wpdb->prefix . "snp_category";

        $rows = $wpdb->get_results("SELECT s.id, s.snpID, s.includeInReport, s.categoryID, c.category from $table_name as s INNER JOIN $table_category_name as c ON s.categoryID=c.id");
        $totoalcnt = count($rows);
		$limit = 5;
		$maxpages = ceil($totoalcnt / $limit);
		$lowid = ($paged - 1) * $limit + 1;
		$maxid = $paged * $limit;
		if($paged > $maxpages)$paged = $maxpages;
		?>
        <table class='wp-list-table widefat fixed striped posts' style="width:100%">
            <tr>
                <th class="manage-column ss-list-width" style="width:20%;text-align:center">SNP</th>
                <th class="manage-column ss-list-width" style="width:20%;text-align:center">include In Report?</th>
                <th class="manage-column ss-list-width" style="width:30%;text-align:center">category</th>
                <th class="manage-column ss-list-width" style="width:20%;text-align:center">&nbsp;</th>
            </tr>
            <?php 
			$i = 0;
			foreach ($rows as $row) { 
				$i++;
				if($i < $lowid)continue;
				if($i > $maxid)break;
			?>
                <tr style="border-bottom-style:inset;border-color:#aaa;">
                    <td class="manage-column ss-list-width" style="text-align:center"><?php echo $row->snpID; ?></td>
                    <td class="manage-column ss-list-width" style="text-align:center">
					<?php if($row->includeInReport == 'on'){ ?>
					<input type="checkbox" name="includeInReport" checked="checked" class="ss-field-width" stye="border-color: rgba(47, 44, 44, 0.75) !important" disabled/> Include
					<?php }else{ ?>
					<input type="checkbox" name="includeInReport" class="ss-field-width" stye="border-color: rgba(47, 44, 44, 0.75) !important;" disabled/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<?php } ?>
					</td>
                    <td class="manage-column ss-list-width" style="text-align:center"><?php echo $row->category; ?></td>
                    <td style="text-align:center"><a href="<?php echo admin_url('admin.php?page=snp_crud_update&id=' . $row->id.'&paged='.$paged); ?>">Update</a></td>
                </tr>
            <?php } ?>
        </table>
		<?php 
		if (function_exists("pagination")) {
			
			pagination($maxpages, $paged, 2);
		} 
		?>
    </div>
	<style>
		.pagination {
		clear:both;
		padding:20px 0;
		position:relative;
		font-size:11px;
		line-height:13px;
		}
		 
		.pagination span, .pagination a {
		display:block;
		float:left;
		margin: 2px 2px 2px 0;
		padding:6px 9px 5px 9px;
		text-decoration:none;
		width:auto;
		color:#fff;
		background: #555;
		}
		 
		.pagination a:hover{
		color:#fff;
		background: #3279BB;
		}
		 
		.pagination .current{
		padding:6px 9px 5px 9px;
		background: #3279BB;
		color:#fff;
		}
	</style>
    <?php
}
function pagination($pages = '',$paged = 1, $range = 4)
{  
     $showitems = ($range * 2)+1;  
 
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\" style='float:right'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}