<?php

function snp_crud_update() {
	@ini_set('display_errors', 0);
    global $wpdb;
    $table_name = $wpdb->prefix . "snp";
    $id = $_GET["id"];
    $snpID = $_POST["snpID"];
	if(isset($_GET['paged'])){
		$paged = $_GET["paged"];
	}else if(isset($_POST['paged'])){
		$paged = $_POST["paged"];
	}else{
		$paged = 1;
	}
	
	
    $includeInReport = $_POST["includeInReport"];
    $categoryID = $_POST["categoryID"];
	if(!isset($_POST['update'])){
		$snp1 = $wpdb->get_results($wpdb->prepare("SELECT id,snpID,includeInReport,categoryID from $table_name where id=%s", $id));
		foreach ($snp1 as $snp) {
			$snpID = $snp->snpID;
			$includeInReport = $snp->includeInReport;
			$categoryID = $snp->categoryID;
		}
	}
    $table_category_name = $wpdb->prefix . "snp_category";
	$categories = $wpdb->get_results($wpdb->prepare("SELECT id, category from $table_category_name"));
//update
    if (isset($_POST['update'])) {
		$wpdb->query("UPDATE $table_name SET snpID='$snpID', includeInReport='$includeInReport',categoryID=$categoryID  WHERE id=$id");
        
    }
//delete
    else if (isset($_POST['delete'])) {
        $wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE id = %s", $id));
    } else {//selecting value to update	
        $snps = $wpdb->get_results($wpdb->prepare("SELECT id,snpID,includeInReport,categoryID from $table_name where id=%s", $id));
        foreach ($snps as $s) {
            $name = $s->name;
        }
    }
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/snp_crud/style-admin.css" rel="stylesheet" />
    <div class="wrap" style="margin-left:15%;width:70%">
        <h2>SNPs</h2>

        <?php if ($_POST['delete']) { ?>
            <div class="updated"><p>SNP deleted</p></div>
            <a href="<?php echo admin_url('admin.php?page=snp_crud_list&paged='.$paged) ?>">&laquo; Back to SNP list</a>

        <?php } else if ($_POST['update']) { ?>
            <div class="updated"><p>SNP updated</p></div>
            <a href="<?php echo admin_url('admin.php?page=snp_crud_list&paged='.$paged) ?>">&laquo; Back to SNP list</a>

        <?php } else { ?>
            <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
                <table class='wp-list-table widefat fixed' style="width:100%">
					<input type="hidden" name="paged" id="paged" value="<?php echo $paged;?>">
                    <tr><th>SNP</th><td><input type="text" name="snpID" id="snpID" value="<?php echo $snpID; ?>"/></td></tr>
                    <tr><th>include In Report?</th><td>
					<input type="checkbox" name="includeInReport" class="ss-field-width" <?php if($includeInReport == 'on'){ echo 'checked="checked"';}?>/> Include
					</td></tr>
                    <tr><th>category</th><td>
					<select name="categoryID" class="ss-field-width">
						<?php foreach( $categories as $category){
								$checked = '';
								if($category->id == $categoryID){
									$checked = 'selected';
								}
						?>
								<option value="<?php echo $category->id; ?>" <?php echo $checked;?>><?php echo $category->category;?></option>
						<?php } ?>
					</select>	
					</td></tr>
                </table>
                <input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
                <input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Do you want really to delete this SNP?')">
            </form>
        <?php } ?>

    </div>
    <?php
}